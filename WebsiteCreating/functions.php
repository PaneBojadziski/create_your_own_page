<?php
// require_once __DIR__ . "/db.php";


// Function that checks if given request is NOT POST, redirecting to form, only post request go through
function checkPostRequest()
{
    if ($_SERVER['REQUEST_METHOD'] != 'POST') {
        header("Location: form.php");
        die();
    }
}

// Function that checks if all the fields have data, if empty than error occurred
function requiredFields() {
    $fields = [
        $_POST['url'], $_POST['title'], $_POST['subtitle'], $_POST['aboutyou'], $_POST['number'], $_POST['location'],
        $_POST['imgurl'], $_POST['desc'], $_POST['imgurl2'], $_POST['desc2'], $_POST['imgurl3'], $_POST['desc3'],
        $_POST['companydesc'], $_POST['linkdin'], $_POST['fb'], $_POST['ig'], $_POST['twitter']
    ];

    foreach($fields as $field) {
        if(empty($field)) {
            $_SESSION['status'] = 'required';
            header("Location: form.php");
            die();
        }
    }
}

// URL validation, checks if the given input in the url fields are really well formed url addresses
function validateURL() {
    $urls = [$_POST['url'], $_POST['imgurl'], $_POST['imgurl2'], $_POST['imgurl3'], $_POST['linkdin'], $_POST['fb'], $_POST['ig'], $_POST['twitter']];

    foreach ($urls as $url) {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            $_SESSION['status'] = 'url';
            header("Location: form.php");
            die();
        } 
    }
}

// Function that checks if the description fields have min 4 words, if not than error occurred
function descFields4WordMin() {
    $descfields = [$_POST['aboutyou'], $_POST['desc'], $_POST['desc2'], $_POST['desc3'], $_POST['companydesc']];

    foreach($descfields as $field) {
        if(str_word_count($field) < 4) {
            $_SESSION['status'] = 'desc';
            header("Location: form.php");
            die();
        }
    }
}

// Function that checks print the error messages from the session
function printErrorMessages()
{
    $messages = [
        'required' => 'All fields are required.',
        'url' => 'The given data is not url.',
        'desc' => 'Description fields must have at least 4 words.',
        'general' => 'An error occurred'
    ];

    if (isset($_SESSION['status'])) {
        $msg = $messages['general'];
        if (isset($messages[$_SESSION['status']])) {
            $msg = $messages[$_SESSION['status']];
        }
        echo "<div class='flex justify-center mt-3'>
        <div class='w-96 text-center bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded' role='alert'>
        <strong class='font-bold'>Error!</strong>
        <span class='block sm:inline'>$msg</span>
      </div>
      </div>";
    }
    unset($_SESSION['status']);
}