<?php
require_once __DIR__ . "/db.php";
require_once __DIR__ . "/functions.php";


session_start();

checkPostRequest();

requiredFields();

validateURL();

descFields4WordMin();


// INFO TABLE FROM DATABASE INSERT, ALSO FETCH THE DATA, ONLY LAST INSERTED DATA WILL BE FETCHED
$sqlinfo = "INSERT INTO info (img_url, main_title, sub_title, about_you, telephone_number, location)
VALUES(:img_url, :main_title, :sub_title, :about_you, :telephone_number, :location)
";
$stmtinfo = $pdo->prepare($sqlinfo);
if($stmtinfo->execute(['img_url' => $_POST["url"], 'main_title' => $_POST["title"], 'sub_title' => $_POST["subtitle"], 'about_you' => $_POST["aboutyou"], 'telephone_number' => $_POST['number'], 'location' => $_POST["location"]])) {
    $selectinfo = "SELECT * FROM info WHERE 1 ORDER BY id DESC LIMIT 1";
    $stmtinfo1 = $pdo->query($selectinfo);
} else {
    header("Location: index.php?status=error");
    die();
}
$info = $stmtinfo1->fetch();


// TYPE TABLE FROM DATABASE INSER, ALSO FETCH THE DATA, ONLY LAST INSERTED DATA WILL BE FETCHED
$sqltype = "INSERT INTO type (type) VALUES (:type)";
$stmttype = $pdo->prepare($sqltype);
if($stmttype->execute(['type' => $_POST['type']])) {
    $selecttype = "SELECT * FROM type WHERE 1 ORDER BY id DESC LIMIT 1";
    $stmttype1 = $pdo->query($selecttype);
}
$type = $stmttype1->fetch();



// SERVICES TABLE INSERT, FETCH THE DATA ALSO, ONLY LAST INSERTED DATA WILL BE FETCHED
$sqlproduct = "INSERT INTO services (type_id, img_url_one, img_desc_one, img_url_two, img_desc_two, img_url_three, img_desc_three) VALUES
(:type_id, :img_url_one, :img_desc_one, :img_url_two, :img_desc_two, :img_url_three, :img_desc_three)
";
$stmtproduct = $pdo->prepare($sqlproduct);
if($stmtproduct->execute(['type_id' => $type['id'], 'img_url_one' => $_POST['imgurl'], 'img_desc_one' => $_POST['desc'], 'img_url_two' => $_POST['imgurl2'], 'img_desc_two' => $_POST['desc2'], 'img_url_three' => $_POST['imgurl3'], 'img_desc_three' => $_POST['desc3']])) {
    $selectproduct = "SELECT * FROM services WHERE 1 ORDER BY id DESC LIMIT 1";
    $stmtproduct1 = $pdo->query($selectproduct);
} else {
    header("Location: index.php?error=product");
    die();
}
$services = $stmtproduct1->fetch();

// COMPANY TABLE INSERT, ALSO FETCH THE DATA, ONLY LAST INSERTED DATA WILL BE FETCHED
$sqlcompany = "INSERT INTO company (info_id, services_id, company_desc, linkdin, facebook, instagram, twitter) VALUES
(:info_id, :services_id, :company_desc, :linkdin, :facebook, :instagram, :twitter)
";
$stmtcompany = $pdo->prepare($sqlcompany);
if($stmtcompany->execute(['info_id' => $info['id'], 'services_id' => $services['id'], 'company_desc' => $_POST['companydesc'], 'linkdin' => $_POST['linkdin'], 'facebook' => $_POST['fb'], 'instagram' => $_POST['ig'], 'twitter' => $_POST['twitter']])) {
    header("Location: template.php");
    die();
    // $selectcompany = "SELECT * FROM company WHERE 1 ORDER BY id DESC LIMIT 1";
    // $stmtcompany1 = $pdo->query($selectcompany);
} else {
    header("Location: form.php?status=company");
    die();
}
// $company = $stmtcompany1->fetch();

