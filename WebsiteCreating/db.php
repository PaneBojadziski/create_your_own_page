<?php
require_once __DIR__ . "/consts.php";

try {
    $pdo = new PDO("mysql:host=127.0.0.1;dbname=" . DBNAME, DBUSER, DBPASS);
} catch (PDOException $e) {
    echo "Database down.";
    die();
}
