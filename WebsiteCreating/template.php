<?php
require_once __DIR__ . "/db.php";
require_once __DIR__ . "/functions.php";

session_start();


$companyinfo = "SELECT info.img_url, info.main_title, info.sub_title, info.about_you, info.telephone_number, info.location, company.company_desc, company.linkdin, company.facebook, company.instagram, company.twitter, services.img_url_one, services.img_desc_one, services.img_url_two, services.img_desc_two, services.img_url_three, services.img_desc_three FROM info 
INNER JOIN company ON info.id = company.info_id
INNER JOIN services ON services.id = company.services_id
ORDER BY company.id DESC LIMIT 1";
$stmtcompany = $pdo->query($companyinfo);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c3264703b7.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Raleway:200,100,400" rel="stylesheet" type="text/css" />
    <title>Document</title>
    <style>
        body {
            font-family: 'Raleway', sans-serif;
            font-size: 18px;
            /* color: #aaa */
        }

        hr {
            width: 50%;
            margin-left: 25%;
            margin-right: 25%;
            height: 10px;
            border: 0;
            box-shadow: 0 10px 10px -10px red inset;
            margin-top: 2%;
            margin-bottom: 2%;
        }

        .input {
            border: 2px solid grey;
            width: 100%;
        }
    </style>
</head>

<body>
    <?php
    $data = $stmtcompany->fetch();
    ?>
    <!-- Navigation bar(need to be responsive) -->
    <nav class="flex items-center flex-wrap p-6">
        <div class="flex items-center flex-shrink-0 text-black mr-6">
            <svg class="fill-current h-8 w-8 mr-2" width="54" height="54" viewBox="0 0 54 54" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.5 22.1c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05zM0 38.3c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05z" />
            </svg>
            <span class="font-semibold text-xl tracking-tight"><?= $data['main_title'] ?></span>
        </div>
        <!-- <div class="block lg:hidden">
            <button class="flex items-center px-3 py-2 border rounded text-teal-200 border-teal-400 hover:text-white hover:border-white">
                <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <title>Menu</title>
                    <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
                </svg>
            </button>
        </div> -->
        <ul class="flex border-b">
            <li class="-mb-px mr-1">
                <a class="bg-white inline-block border-l border-t border-r rounded-t py-2 px-4 text-red-700 font-semibold" href="#">Home</a>
            </li>
            <li class="mr-1">
                <a class="bg-white inline-block py-2 px-4 text-red-500 hover:text-red-800 font-semibold" href="#">About Us</a>
            </li>
            <li class="mr-1">
                <a class="bg-white inline-block py-2 px-4 text-red-500 hover:text-red-800 font-semibold" href="#">Services</a>
            </li>
            <li class="mr-1">
                <a class="bbg-white inline-block py-2 px-4 text-red-500 hover:text-red-800 font-semibold" href="#">Contact</a>
            </li>
        </ul>
    </nav>

    <!-- Image width 600 -->
    <div class="flex justify-center">
        <img class="w-full max-h-screen" src="<?= $data['img_url'] ?>" alt="header image" />
        <div class="absolute mt-20 text-white text-5xl">
            <h1>This pen is
                <span class="txt-rotate" data-period="2000" data-rotate='[ "<?= $data['main_title'] ?>", "simple.", "pure JS.", "pretty.", "fun!" ]'></span>
            </h1>
        </div>

        <div class="absolute mt-96 text-white text-2xl">
            <h4><?= $data['sub_title'] ?></h4>
        </div>
    </div>

    <!-- Abouts us and contact part  -->
    <div class="flex justify-center text-black text-3xl font-bold mt-4">
        <h2>Abouts Us</h2>
    </div>

    <div class="flex justify-center text-black">
        <p><?= $data['about_you'] ?></p>
    </div>

    <hr>

    <div class="flex justify-center">
        <p><i class="pr-1 fas fa-phone"></i>Tel: <?= $data['telephone_number'] ?></p>
    </div>

    <div class="flex justify-center">
        <p><i class="pr-1 fas fa-location-arrow"></i>Location: <?= $data['location'] ?></p>
    </div>

    <!-- Services part -->
    <div class="flex justify-evenly mt-10 mb-10">

        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-full" src="<?= $data['img_url_one'] ?>" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2"><?= $data['main_title'] ?></div>
                <p class="text-gray-700 text-base">
                    <?= $data['img_desc_one'] ?>
                </p>
            </div>
            <div class="px-6 pt-4 pb-2">
                <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#sunglasses</span>
                <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#fancy</span>
                <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#cool</span>
            </div>
        </div>

        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-full" src="<?= $data['img_url_two'] ?>" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2"><?= $data['main_title'] ?></div>
                <p class="text-gray-700 text-base">
                    <?= $data['img_desc_two'] ?>
                </p>
            </div>
            <div class="px-6 pt-4 pb-2">
                <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#sun</span>
                <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#perfect</span>
                <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#tasty</span>
            </div>
        </div>

        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-full" src="<?= $data['img_url_three'] ?>" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2"><?= $data['main_title'] ?></div>
                <p class="text-gray-700 text-base">
                    <?= $data['img_desc_three'] ?>
                </p>
            </div>
            <div class="px-6 pt-4 pb-2">
                <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#yammy</span>
                <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#feeling</span>
                <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#passion</span>
            </div>
        </div>

    </div>

    <hr>

    <!-- Contact us form  -->

    <div class="grid grid-rows-2 grid-cols-2 gap-4">
        <div class="row-span-2">
            <p><span class="font-bold">Why you should contact us and leave you information?</span> Well we are one of the best growing companies in the city(soon in the country) and we care what you the customers think for us.
                From your feedback we can improve our self and we can improve the taste of the services/product that we offer. So this is simple, you shoud leave you name and email and the message/feedback you have for us,
                after our reviewing we can see what is the point that we missed speak with our menagers and improve ourselfs. Thank you. <span class="font-bold"><?= $data['main_title'] ?> </span> is wishing you a productive rest of the day.
            </p>
        </div>
        <div class="row-span-2">
            <form action="contactus.php" method="POST">
                <label for="name">Name</label><br />
                <input class="input" type="text" name="name" id="name" placeholder="Your name..."><br />
                <label for="email">Email</label><br />
                <input class="input" type="email" name="email" id="email" placeholder="Your email..."><br />
                <label for="message">Message</label><br />
                <textarea class="input" name="message" id="message" rows="10">Enter your feedback here...</textarea><br />
                <div class="flex justify-center">
                    <button class="text-xl bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded-full">Send</button>
                </div>
            </form>
        </div>
    </div>

    <!-- Footer part -->
    <footer>
        <div class="h-20 bg-black text-white flex justify-center mt-10">
            <p class="mt-2">Copyright by Pane &copy;</p>
            <p class="absolute mt-10">
                <a href="<?= $data['linkdin'] ?>" target="_blank"><i class="pr-2 fab fa-linkedin fa-2x"></i></a>
                <a href="<?= $data['facebook'] ?>" target="_blank"><i class="pr-2 fab fa-facebook fa-2x"></i></a>
                <a href="<?= $data['instagram'] ?>" target="_blank"><i class="pr-2 fab fa-instagram fa-2x"></i></a>
                <a href="<?= $data['twitter'] ?>" target="_blank"><i class="fab fa-twitter fa-2x"></i></a>
            </p>
        </div>
    </footer>






    <script>
    </script>
</body>

</html>