<?php

session_start();
require_once __DIR__ . "/functions.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <style>
        body {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            background: url(images/milky-way-2695569_960_720.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
</head>

<body>
    <form class="bg-purple-900 shadow-md rounded px-8 pt-6 pb-8 mb-4 m-32" action="website.php" method="POST">
        <div class="grid">
            <h1 class="mt-7 justify-self-center text-white text-3xl">You are one step away from your website</h1>
        </div>
        <?php
        printErrorMessages();
        ?>
        <div class="mt-5 flex justify-evenly mx-32 border-dotted border-4 border-light-blue-500">

            <div>
                <label class="text-white" for="coverimg">Cover image URL</label><br />
                <input class="border-solid" type="url" name="url" id="coverimg"><br /> <br />
                <label class="text-white" for="title">Main Title of Page</label><br />
                <input type="text" name="title" id="title"><br /> <br />
                <label class="text-white" for="subtitle">Subtitle for Page</label><br />
                <input type="text" name="subtitle" id="subtitle"><br /> <br />
                <label class="text-white" for="aboutyou">Something about you</label><br />
                <textarea name="aboutyou" id="aboutyou" cols="25" rows="3"></textarea><br /> <br />
                <label class="text-white" for="number">You telephone number</label><br />
                <input type="number" name="number" id="number"><br /> <br />
                <label class="text-white" for="location">Location</label><br />
                <input type="text" name="location" id="location"><br /> <br />
                <label class="text-white" for="services">Do you provide <br />services or product?</label><br />
                <select class="pr-24" name="type" id="type">
                    <option value="service" selected>Services</option>
                    <option value="product">Product</option>
                </select>
            </div>

            <div>
                <small class="text-purple-300">Provide url for an image and description <br />of your service/product</small><br /> <br />
                <label class="text-white" for="imgurl">Image URL</label><br />
                <input class="border-solid" type="url" name="imgurl" id="imgurl"><br /> <br />
                <label class="text-white" for="desc">Description of your service/product</label><br />
                <textarea name="desc" id="desc" cols="25" rows="3"></textarea><br /> <br />
                <label class="text-white" for="imgurl2">Image URL</label><br />
                <input class="border-solid" type="text" name="imgurl2" id="imgurl2"><br /> <br />
                <label class="text-white" for="desc2">Description of your service/product</label><br />
                <textarea name="desc2" id="desc2" cols="25" rows="3"></textarea><br /> <br />
                <label class="text-white" for="imgurl3">Image URL</label><br />
                <input class="border-solid" type="text" name="imgurl3" id="imgurl3"><br /> <br />
                <label class="text-white" for="desc3">Description of your service/product</label><br />
                <textarea name="desc3" id="desc3" cols="25" rows="3"></textarea><br /> <br />
            </div>

            <div>
                <small class="text-white">Provide a description of your company <br />something people should be aware of <br />before they contack you:</small><br />
                <textarea name="companydesc" id="companydesc" cols="25" rows="3"></textarea><br /> <br />
                <label class="text-white" for="imgurl">LinkdIn</label><br />
                <input class="border-solid" type="url" name="linkdin" id="linkid"><br /> <br />
                <label class="text-white" for="fb">Fecebook</label><br />
                <input class="border-solid" type="url" name="fb" id="fb"><br /> <br />
                <label class="text-white" for="ig">Instagram</label><br />
                <input class="border-solid" type="url" name="ig" id="ig"><br /> <br />
                <label class="text-white" for="twitter">Twitter</label><br />
                <input class="border-solid" type="url" name="twitter" id="twitter"><br /> <br />
            </div>

        </div>
        <div class="flex justify-center mt-4">
            <button class="w-96 h-12 px-6 text-white-300 transition-colors duration-150 bg-purple-300 rounded-lg focus:shadow-outline hover:bg-indigo-800">Submit</button>
        </div>
    </form>
</body>

</html>