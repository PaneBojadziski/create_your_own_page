<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <title>Document</title>
    <style>
        body {
            font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        }
    </style>
</head>

<body>
    <div class="grid absolute inset-20 bg-gradient-to-r from-cyan-400 to-sky-500 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl">
        <img class="absolute sm:rounded-3xl" src="images/1_0FqDC0_r1f5xFz3IywLYRA.jpeg" alt="programmer-pic" width="870">
        <h1 class="absolute uppercase text-yellow-600 font-bold ml-5 text-2xl">Create your own website, just fill up a form</h1>
        <h2 class="absolute uppercase mt-7 ml-9 text-yellow-600 font-bold text-lg">click the <span class="underline text-2xl">Start</span> button to get started</h2>
        <form class="mx-16 absolute justify-self-end self-center" action="form.php" method="POST">
        <button class="text-2xl mx-20 uppercase bg-transparent hover:bg-yellow-200 text-yellow-500 font-semibold hover:text-white py-10 px-20 border border-yellow-500 hover:border-transparent rounded">
            Start
        </button>
        </form>
    </div>
</body>

</html>